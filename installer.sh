#!/bin/bash
set -ue

BASHRC_LOC="${HOME}/.bashrc"

whereami="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
email=""
get_email() {
    echo "please provide an email address for Run On Complete Script"
    read -r email
}

install_amix_vim() {
    if [ ! -f ~/.vim_runtime/install_awesome_vimrc.sh ]; then
        git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
        sh ~/.vim_runtime/install_awesome_vimrc.sh
        echo "set number" >> ~/.vimrc
    fi
}

add_mod_string(){
    modsString="
##mods for resources install start
export EMAIL=\"$email\"
export RESOURCEDIR=\"$whereami\"
source \"\$RESOURCEDIR/TerminalMods/sourcedFiles/bashrcMods\"
export PATH=\"\$RESOURCEDIR/TerminalMods/programs:\$PATH\"
##mods for resources install end
    	"
    echo -e "$modsString" >> ~/.bashrc
}

get_line_num_from_grep(){
    grep -n "$1" "$BASHRC_LOC" | cut -f1 -d:
}

rm_modstring_if_exist(){

    startLine="##mods for resources install start"

    if ! grep "$startLine" "$BASHRC_LOC" ;then
        return 0
    fi

    endLine="##mods for resources install end"


    startLine=$(get_line_num_from_grep "$startLine")
    endLine=$(get_line_num_from_grep "$endLine")

    echo "$startLine"
    echo "$endLine"

    sed -i.bak "$startLine,${endLine}d" "$BASHRC_LOC"
}

add_folders() {
    #Make trash folder if no exist
    mkdir -p ~/.local/share/Trash/{files,info} "$HOME/bin" "$HOME/Downloads"
}
copy_input_rc() {
    #if I make any changes to inputrc, they'll get copied over because the file will be overwritten
    cp "$whereami/inputrc" "$HOME/.inputrc"
}
install_caddy() {
    caddy_url='https://caddyserver.com/download/linux/amd64?license=personal&telemetry=on'
    wget "$caddy_url" -O "$HOME/Downloads/caddy.tar.gz"
    tar -xvf "$HOME/Downloads/caddy.tar.gz" -C "$HOME/bin" caddy
}

add_global_gitignore(){
    git config --global core.excludesfile ~/.gitignore_global
    cp "$whereami/gitignore_global" "$HOME/.gitignore_global"
}
increase_inotify(){
    echo -n "Want to increase iNotify limit(Seafile, Webstorm)? [y/N]: "
    read response
    if [ "$response" = "y" ]; then
        #copied from https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers
        echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
    fi
}

main() {

    if echo "$@" | grep '\-e'; then
        get_email
    fi
    rm_modstring_if_exist
    add_mod_string
    add_folders
    copy_input_rc
    add_global_gitignore
    install_amix_vim
    install_caddy
    increase_inotify
}
main "$@"
